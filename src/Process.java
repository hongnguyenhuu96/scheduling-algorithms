/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author hongn
 */
public class Process {
    protected String name;
    protected double arivalTime;
    protected double burstedTime;
    protected static int numOfProcess;
    protected double startTime; // thoi gian bat dau tien trinh
    protected double finishedTime; // thoi gian ket thuc tien trinh
    protected double remainningTime; // thoi gian con lai khi chay loai 3 vs 4
    
    Process(int name1, double arivalTime1, double burstedTime1){
        this.name = "P" + name1;
        this.arivalTime = arivalTime1;
        this.burstedTime = burstedTime1;
        this.remainningTime = burstedTime1;
        this.startTime = -1;
        this.finishedTime = -1;
    }
    
    public double getTurnAroundTime(){
        return finishedTime - arivalTime;
    }
    
    public double getWaitingTime(){
        return finishedTime - arivalTime - burstedTime;
    }
    public double getRespondTime(){
        return startTime - arivalTime;
    }
}
