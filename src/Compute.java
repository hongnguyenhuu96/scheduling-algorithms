
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author hongn
 */
public class Compute {
    static String log = "";

    public static String getThroughput(Process[] p) {
        int totalBurstedTime = 0;
        for (int i = 0; i < p.length; i++) {
            totalBurstedTime += p[i].burstedTime;
        }
        return "Throughput = " + p.length + "/" + totalBurstedTime + " = " + (double) p.length / totalBurstedTime;
    }

    public static String getAverageWaitingTime(Process[] p) {
        double totalWaitingTime = getTotalWatingTime(p);
        return "Average waiting time: " + totalWaitingTime + "/" + p.length + " = " + totalWaitingTime / (double) p.length;
    }

    public static double getTotalWatingTime(Process[] p) {
        double totalWaitingTime = 0;
        for (int i = 0; i < p.length; i++) {
            totalWaitingTime += p[i].getWaitingTime();
        }
        return totalWaitingTime;
    }

    public static String getAverageRespondTime(Process[] p) {
        double totalRespondTime = getTotalRespondTime(p);
        return "Average respond time: " + totalRespondTime + "/" + p.length + " = " + totalRespondTime / (double) p.length;
    }

    public static double getTotalRespondTime(Process[] p) {
        double totalRespondTime = 0;
        for (int i = 0; i < p.length; i++) {
            totalRespondTime += p[i].getRespondTime();
        }
        return totalRespondTime;
    }

    public static String runFirstComeFirstServe(Process[] p) {
        log = "";
        sortProcessByArivalTime(p);
        p[0].startTime = p[0].arivalTime;
        p[0].finishedTime = p[0].startTime + p[0].burstedTime;
        p[0].remainningTime = 0;
        
        for (int i = 1; i < p.length; i++) {
            if (p[i].arivalTime < p[i - 1].finishedTime) {
                p[i].startTime = p[i - 1].finishedTime;
            } else {
                p[i].startTime = p[i].arivalTime;
            }
            p[i].finishedTime = p[i].startTime + p[i].burstedTime;
            p[i].remainningTime = 0;
        }
        writeLog(p);
        return log;
    }

    public static String runSortestJumpNext(Process[] p) {
        log = "";
        // neu ko co process thi return luon
        if (p.length == 0 || p == null) {
            return log;
        }
        sortProcessByArivalTime(p);
        p[0].startTime = p[0].arivalTime;
        p[0].finishedTime = p[0].startTime + p[0].burstedTime;
        p[0].remainningTime = 0;
        writeLog(p);
        double currentTime = p[0].finishedTime;

        // neu co nhieu hon 1 process
        for (int k = 1; k < p.length; k++) {
            /// find the next process to run
            double min = Double.MAX_VALUE;
            // neu ko co tien trinh nao xuat hien den thoi diem hien tai
            // chay tien trinh dau tien xuat hien trong tuong lai (arival_time)
            int nextProcess = k;
            // tim tien trinh chua duoc chay va da xuat hien co burstedTime nho nhat
            for (int i = 1; i < p.length; i++) {
                if (p[i].startTime == -1 && p[i].arivalTime <= currentTime) { // chua duoc chay va da xuat hien
                    if (p[i].burstedTime < min) {
                        min = p[i].burstedTime;
                        nextProcess = i;
                    }
                }
            }

            // chay tien trinh tiep theo tim duoc phia tren
            if (p[nextProcess].arivalTime > currentTime) {
                p[nextProcess].startTime = p[nextProcess].arivalTime;
            } else {
                p[nextProcess].startTime = currentTime;
            }
            p[nextProcess].finishedTime = p[nextProcess].startTime + p[nextProcess].burstedTime;
            p[nextProcess].remainningTime = 0;
            currentTime = p[nextProcess].finishedTime;
            writeLog(p);
        }
        return log;
    }

    public static String runSortestRemainningTimeFirst(Process[] p) {
        log = "";
        if (p.length == 0 || p == null) {
            return log;
        }
        sortProcessByArivalTime(p);
        double currentTime = p[0].arivalTime;
        while (true) {
            int nextArival = -1; // chua biet cai nao xuat hien tiep theo
            int runNext = 0;
            // tim thang tiep theo se chay: 
            //la thang co thoi gian xuat hien < thoi diem hien tai
            // va co thoi gian chay con lai != 0
            boolean flag = false; // danh dau tim duoc process da xuat hien chua chay het va co remainning time nho nhat
            double minRemainningTime = Double.MAX_VALUE;
            for (int i = 0; i < p.length; i++) {
                if (p[i].arivalTime <= currentTime && p[i].remainningTime != 0) {
                    if (p[i].remainningTime < minRemainningTime) {
                        flag = true;
                        minRemainningTime = p[i].remainningTime;
                        runNext = i;
                    }
                }
            }
            // p[runnext] se la process da xuat hien co remainning time nho nhat hoac xuat hien tiep theo

            // chay
            if (flag) { //neu van con process xuat hien roi chua chay xong
                if (p[runNext].startTime == -1) { // neu process chua tung chay
                    p[runNext].startTime = currentTime;
                }
            } else { // process xuat hien chay het roi
                //can tim process xuat hien dau tien trong tuong lai de chay tiep theo
                boolean conti = false;
                for (int i = 0; i < p.length; i++) {
                    if (p[i].arivalTime > currentTime) {
                        runNext = i;
                        conti = true;
                        break;
                    }
                }
                // neu tat ca cac process da chay het, khong con process nao trong tuong lai
                if (conti == false) {
                    return log;
                }
                // neu van con process trong tuong lai
                p[runNext].startTime = p[runNext].arivalTime;
                currentTime = p[runNext].arivalTime;
            }

            // neu dang chay: co the gap 1 thang khac, hoac ko gap thang nao
            // tim thang xuat hien ke tiep tu thoi diem hien tai
            for (int i = 0; i < p.length; i++) {
                if (p[i].arivalTime > currentTime) {
                    nextArival = i;
                    break;
                }
            }
            if (nextArival == -1) { // neu ko tim duoc, tat ca process deu da duoc chay, trong tuong lai ko con thang nao chua tung chay ca
                //chay not process hien tai
                p[runNext].finishedTime = currentTime + p[runNext].remainningTime;
                p[runNext].remainningTime = 0;
                currentTime = p[runNext].finishedTime;
            } else // neu tim duoc thang nao do chay trong tuong lai
            {
                if (p[runNext].remainningTime + currentTime > p[nextArival].arivalTime) {
                    // neu dang chay gap 1 process khac thi dung tinh toan tiep
                    p[runNext].remainningTime = p[runNext].remainningTime - (p[nextArival].arivalTime - currentTime);
                    currentTime = p[nextArival].arivalTime;
                } else { // neu dang chay ko gap process khac thi chay not
                    p[runNext].finishedTime = currentTime + p[runNext].remainningTime;
                    p[runNext].remainningTime = 0;
                    currentTime = p[runNext].finishedTime;
                }
            }
            writeLog(p);
        }
    }

    public static String runRoundRobin(Process p[], double timeQuantum) {
        log = "";
        ArrayList<Process> apearredProcess = new ArrayList<Process>();
        sortProcessByArivalTime(p);
        
        apearredProcess.add(p[0]);
        double currentTime = p[0].arivalTime;

        int numFinish = 0;
        while (true) {
            for (int i = 0; i < apearredProcess.size(); i++) {
                // gan de su dung cho ngan trong doan code
                Process cP = apearredProcess.get(i); // cP: currentProcess;
                if(cP.remainningTime == 0) continue;
                // neu process chua duoc chay truoc do thi cai dat thoi gian bat dau
                // la thoi gian hien tai
                if (cP.startTime == -1) {
                    cP.startTime = currentTime;
                }
                // tinh toan thoi gian chay cua process hien tai
                double timeRun = 0;
                if (cP.remainningTime < timeQuantum) {
                    timeRun = cP.remainningTime;
                } else {
                    timeRun = timeQuantum;
                }
                // tinh toan cac chi so moi sau khi chay thoi gian timeRun
                currentTime += timeRun;
                cP.remainningTime -= timeRun;
                if (cP.remainningTime == 0) {
                    cP.finishedTime = currentTime;
                    numFinish++;
                    if(numFinish == p.length){
                        writeLog(p);
                        return log;
                    }
                }
                // tinh toan xem co them cai nao moi xuat hien trong luc chay vao
                // queue de chay trong vong tiep theo ko
                // neu co se la process cho chi so = apearredProcess.size()
                if (apearredProcess.size() + 1 <= p.length) {
                    if (currentTime >= p[apearredProcess.size()].arivalTime) {
                        apearredProcess.add(p[apearredProcess.size()]);
                    }
                }
                
                int numApearredProcess = apearredProcess.size();
                for(int k = numApearredProcess ; k < p.length; k++){
                    if(currentTime >= p[k].arivalTime){
                        apearredProcess.add(p[k]);
                    }
                }
                writeLog(p);
            }
        }
    }

    // 2 phan sort nay phai co gang cai tien thanh template
    protected static void sortProcessByArivalTime(Process[] p) {
        Process temp;
        for (int i = 0; i < p.length - 1; i++) {
            for (int j = i + 1; j < p.length; j++) {
                if (p[j].arivalTime < p[i].arivalTime) {
                    temp = p[i];
                    p[i] = p[j];
                    p[j] = temp;
                }
            }
        }
    }

    protected static void sortProcessByName(Process[] p) {
        Process temp;
        for (int i = 0; i < p.length - 1; i++) {
            for (int j = i + 1; j < p.length; j++) {
                if (Integer.parseInt(p[j].name.substring(1)) < Integer.parseInt(p[i].name.substring(1))) {
                    temp = p[i];
                    p[i] = p[j];
                    p[j] = temp;
                }
            }
        }
    }

    private static void showAtribute(Process[] p) {
        System.out.println("___________________");
        for (int i = 0; i < p.length; i++) {
            System.out.println(p[i].name + " start: " + p[i].startTime + " finish: " + p[i].finishedTime + " remaining: " + p[i].remainningTime);
        }
        System.out.println("___________________");
    }
    
    private static void writeLog(Process []p){
        for (int i = 0; i < p.length; i++) {
            String startTime = p[i].startTime + "";
            String finishedTime = p[i].finishedTime + "";
            if(p[i].startTime == -1) startTime = "not";
            if(p[i].finishedTime == -1) finishedTime = "not";
            log += p[i].name + " start: " + startTime + " | finish: " + finishedTime + " | remaining: " + p[i].remainningTime + "\n";
        }
        log += "_______________________________________\n";
    }
    
}



//public static void runRoundRobin2(Process[] p, double timeQuantum) {
//        sortProcessByArivalTime(p);
//        // assum the all the process will start at 0
//        double currentTime = 0;
//        int count = 0;
//        while (count <= p.length) {
//            for (int i = 0; i < p.length; i++) {
//                if (p[i].remainningTime != 0) {
//                    if (p[i].startTime == -1) {
//                        p[i].startTime = currentTime;
//                    }
//                    if (p[i].remainningTime <= timeQuantum) {
//                        currentTime += p[i].remainningTime;
//                        p[i].remainningTime = 0;
//                        p[i].finishedTime = currentTime;
//                        count++;
//                    } else {
//                        p[i].remainningTime -= timeQuantum;
//                        currentTime += timeQuantum;
//                    }
//                }
//            }
//            if (count == p.length) {
//                count++; // neu da chay het cac process thi tang count len de dung chuong trinh
//            }//            showAtribute(p);
//        }
//    }